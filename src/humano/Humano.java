package humano;

public class Humano {

    private String Nombre;
    private String Apellido;
    private int Edad;
    private double Peso;
    private double Altura;
    private double Imc;
   //atributos    
    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    public String getApellido() {
        return Apellido;
    }
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }
    public int getEdad() {
        return Edad;
    }
    public void setEdad(int Edad) {
        this.Edad = Edad;
    }
    public double getPeso() {
        return Peso;
    }
    public void setPeso(double Peso) {
        this.Peso = Peso;
    }
    public double getAltura() {
        return Altura;
    }
    public void setAltura(double Altura) {
        this.Altura = Altura;
    }
    public double getImc() {
        return Imc;
    }
    public void setImc(double Imc) {
        this.Imc = Imc;
    }
    public double calculo(){
    double Imc;
    Imc= this.Peso/Altura*Altura;
    return Imc;
    }
    public String Imc(){
    if(Imc<18.5){
            return "peso insufuciente";
    }
        else if(this.Imc<18.5){
            return "peso normal";
        }
            else if(this.Imc<26.9){
               return "sobrepeso grado 1";
                }
            else if(this.Imc<29.9){
                return "sobrepeso grado 2";}
            else {
            return "obesidad"
                    ;}
    
    }
    //constructor
    public Humano(String N, String A){
    this.Nombre= N;
    this.Apellido = A;
    this.Edad = 0;
    this.Peso = 0;
    this.Imc = 0;
    }
 
    public static void main(String[] args) {
       Humano h = new Humano("andres","escobar");
        System.out.println(" Su nombre es: "+h.getNombre());
        
        System.out.println(" su apellido es: "+h.getApellido());
        h.setEdad(12);
        System.out.println(" su edad es: "+h.getEdad()+" años");
        h.setPeso(48.5);
        System.out.println(" su peso es: "+h.getPeso()+" Lbs");
        h.setAltura(120);
        System.out.println(" su altura es: "+h.getAltura()+" cms");
        
        System.out.println(" el concepto de masa es: "+h.calculo());
        
    } 
}